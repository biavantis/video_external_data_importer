from src import utils
from src import config
from src import actions

import traceback
import datetime


def main_handler(bucket_name, file_key, source_name, src_caller):

    global_params = {"src_caller": src_caller, "bucket_name": bucket_name,
                     "file_key": file_key, "source_name": source_name}

    source_config = config.source_config[source_name]
    df = utils.read_csv_to_df(global_params, source_config["read_csv_params"])
    df = actions.actions_handling(df, source_config["actions"], global_params)
    df['insert_timestamp_utc'] = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f")
    utils.write_to_db(df, source_config["write_to_db"])


def manual_handler():
    bucket_name = '/home'
    file_key = 'omri/Downloads/apn_24.csv'
    source_name = 'appnexus'

    main_handler(bucket_name, file_key, source_name, 'manual_handler')


def lambda_handler(event, context):
    bucket_name = event.get('Records')[0].get('s3').get('bucket').get('name')
    file_key = event.get('Records')[0].get('s3').get('object').get('key')
    file_key_lst = file_key.split('/')
    source_name = file_key_lst[1]
    print("report name: ", source_name)
    if config.source_config[source_name]["enable"]:
        main_handler(bucket_name, file_key, source_name, 'lambda_handler')


if __name__ == '__main__':
    try:
        manual_handler()
    except Exception:
        print("run failed")
        print(traceback.format_exc())
