from src import utils


def rename_headers(df, params):
    """rename dataframe headers according to list in config"""

    df.columns = params["df_cols_lst"]
    return df


def parse_ad_source(df, params):
    """
    parsing ad. source name/publisher channel name columns:
    1.replace unwanted chars and spacing
    2.lower the string
    :param df:
    :param params:
    :return:
    """
    def parse_col(x):
        x = x.replace("\\", "_")
        x = x.replace(" ", "")
        return x.lower()

    df[params["col_name"]] = df[params["col_name"]].apply(lambda x: parse_col(x))

    return df


def parse_from_col(df, params):
    """extract exchange and device from column"""

    def extract_by_match(x, d):
        for key in d.keys():
            if x.lower().find(key) != -1:
                return d[key]
        return "unknown"

    df[params["new_col_name"]] = df[params["col2search"]].apply(lambda x: extract_by_match(x, params["substr_options"]))

    return df


def parse_domain(df, params):
    """parse site name from domain"""
    df[params["new_col_name"]] = df[params["col_name"]].apply(lambda x: (utils.get_site_name_of_url(x)).lower())
    return df


def check_if_hive(df, params):
    """
    check if domain belongs to hive domains according to hive domains list in config
    :param df:
    :param params:
    :return:  1 - hive domain
              0 - not hive domain
             -1 - an error occurred during checking
    """
    def is_hive(x, hive_lst):
        if not isinstance(x, str):
            return -1
        for domain in hive_lst:
            if x.lower().find(domain) != -1:
                return 1
        return 0

    df[params["new_col_name"]] = df[params["col2search"]].apply(lambda x: is_hive(x, params["hive_lst"]))

    return df


def parse_date_and_time(df, params):
    """parse date and time from timestamp column, then drop timestamp"""

    df['date'] = df[params["col_name"]].apply(lambda x: x.split()[0])
    df['hour'] = df[params["col_name"]].apply(lambda x: x.split()[1])
    df.drop(params["col_name"], inplace=True, axis=1)

    return df


def substr_col(df, params):
    """create a new column by extracting data from existing column"""

    char2find = params["char2find"]

    df[params["col_name"]] = df[params["col_name"]].apply(lambda x: x[:x.rfind(char2find)])
    return df


def do_action(df, action, params):
    """map action to the corresponding function"""
    if action == "rename_headers":
        return rename_headers(df, params)
    if action == "parse_ad_source":
        return parse_ad_source(df, params)
    if action == "parse_from_col":
        return parse_from_col(df, params)
    if action == "parse_domain":
        return parse_domain(df, params)
    if action == "check_if_hive":
        return check_if_hive(df, params)
    if action == "parse_date_and_time":
        return parse_date_and_time(df, params)
    if action == "substr_col":
        return substr_col(df, params)


def actions_handling(df, action_lst, global_params):
    """for each action in config actions, send to 'do_action' function"""
    for action in action_lst:
        action["params"].update(global_params)
        df = do_action(df, action["action"], action["params"])
    return df
