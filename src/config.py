hive_domains = ["trend-chaser.com", "bavardist.com", "brakeforit.com", "themystique.com", "giveitlove.com", "hooch.net",
                "desafiomundial.com", "idolator.com", "postfun.com", "pastfactory.com", "buzznet.com",
                "engage.salon.com", "trending.thechive.com", "bleacherbreaker.com",
                "quizscape.com", "healthygem.com", "purevolume.com", "auntyacid.com", "ricklaxpresents.com",
                "kyoot.com", "moneypop.com", "dadpatrol.com", "gamedaynews.com", "tacorelish.com", "dailyfunny.com",
                "thisiswhyimsingle.com", "factable.com", "exploredplanet.com", "lifestylelatino.com",
                "vidabrilhante.com"]


device_options = {'_mobile_': "Mobile", "_mw_": "Mobile","_tablet_": "Tablet","_desktop_": "Desktop",
                  "_mobile web_": "Mobile","_mobilefinf_": "Mobile", "_mobilesinf_": "Mobile","_desktopreg_": "Desktop",
                  "_desktopajax_": "Desktop", "_mobileaj_": "Mobile","_tabletaj_": "Tablet","_mobilereg_": "Mobile",
                  "_mobileinf_": "Mobile", "desktop": "Desktop", "mobile": "Mobile"
                  }

source_config = {"aniview_revenue": {"enable": True,
                                 "read_csv_params": {"skiprows": None, "keep_default_na": False},
                                 "actions": [
                                     {"action": "rename_headers",
                                      "params": {"df_cols_lst": ['date', 'hour', 'publisher_channel_id',
                                                                 'publisher_channel_name','ad_source_id',
                                                                 'ad_source_name', 'domain','advertiser_name',
                                                                 'impression', 'cpm', 'revenue']}},
                                     {"action": "parse_ad_source", "params": {"col_name": "publisher_channel_name"}},
                                     {"action": "parse_ad_source", "params": {"col_name": "ad_source_name"}},
                                     {"action": "parse_from_col",
                                      "params": {"col2search": "ad_source_name", "new_col_name": "device",
                                                 "substr_options": device_options
                                                 }},
                                     {"action": "parse_from_col",
                                      "params": {"col2search": "advertiser_name", "new_col_name": "exchange",
                                                 "substr_options": {"appnexus": "appnexus", "aol": "aol",
                                                                    "spotx": "spotx", "openx": "openx",
                                                                    "pubmatic": "pubmatic", "index": "index",
                                                                    "freewheel": "freewheel",
                                                                    "beachfront": "beachfront"
                                                                    }
                                                 }},
                                     {"action": "parse_domain", "params": {"new_col_name": "site_name",
                                                                           "col_name": "domain"}},
                                     {"action": "check_if_hive", "params": {"new_col_name": "is_hive",
                                                                            "col2search": "domain",
                                                                            "hive_lst": hive_domains}}
                                 ],
                                 "write_to_db": {"tbl_name": "video.domain_analysis.aniview_revenue",
                                                 "cols_lst": ['date', 'hour', 'publisher_channel_id',
                                                              'publisher_channel_name','ad_source_id', 'ad_source_name',
                                                              'domain','advertiser_name', 'impression', 'cpm', 'revenue',
                                                              'device', 'exchange', 'site_name', 'is_hive',
                                                              'insert_timestamp_utc']
                                                 }
                                 },
                 "aniview_inventory": {"enable": True,
                                 "read_csv_params": {"skiprows": None, "keep_default_na": False},
                                 "actions": [{"action": "rename_headers",
                                              "params": {"df_cols_lst": ['date', 'hour', 'publisher_channel_id',
                                                                         'publisher_channel_name', 'domain',
                                                                         'inventory']}},
                                             {"action": "parse_ad_source",
                                              "params": {"col_name": "publisher_channel_name"}},
                                             {"action": "parse_from_col",
                                              "params": {"col2search": "publisher_channel_name", "new_col_name": "device",
                                                         "substr_options": device_options
                                                         }},
                                             {"action": "parse_domain", "params": {"new_col_name": "site_name",
                                                                                   "col_name": "domain"}},
                                             {"action": "check_if_hive", "params": {"new_col_name": "is_hive",
                                                                                    "col2search": "domain",
                                                                                    "hive_lst": hive_domains}}
                                             ],
                                 "write_to_db": {"tbl_name": "video.domain_analysis.aniview_inventory",
                                                 "cols_lst": ['date', 'hour', 'publisher_channel_id',
                                                              'publisher_channel_name', 'domain', 'inventory',
                                                              'device', 'site_name', 'is_hive',
                                                              'insert_timestamp_utc']
                                                 }
                                 },
                 "appnexus_revenue": {"enable": True,
                              "read_csv_params": {"skiprows": None, "skipfooter": 0, "engine": "python"},
                              "actions": [{"action": "rename_headers",
                                           "params": {"df_cols_lst": ['timestamp', 'ad_source_name', 'domain',
                                                                      'impression', 'revenue', 'cpm']}},
                                          {"action": "parse_ad_source", "params": {"col_name": "ad_source_name"}},
                                          {"action": "substr_col", "params": {"col_name": "ad_source_name",
                                                                              "char2find": "("}},
                                          {"action": "parse_from_col",
                                           "params": {"col2search": "ad_source_name", "new_col_name": "device",
                                                      "substr_options": device_options
                                                      }},
                                          {"action": "parse_domain", "params": {"new_col_name": "site_name",
                                                                                "col_name": "domain"}},
                                          {"action": "check_if_hive", "params": {"new_col_name": "is_hive",
                                                                                 "col2search": "domain",
                                                                                 "hive_lst": hive_domains}},
                                          {"action": "parse_date_and_time", "params": {"col_name": "timestamp"}}

                                          ],
                                      "write_to_db": {"tbl_name": "video.domain_analysis.appnexus_revenue",
                                              "cols_lst": ['ad_source_name', 'domain', 'impression', 'revenue',
                                                           'cpm', 'device', 'site_name', 'is_hive', 'date', 'hour',
                                                           'insert_timestamp_utc']
                                              }
                                      }
                 }
