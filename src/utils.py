from src import config
import pandas as pd
import os
import boto3
import io
import urllib.request
import snowflake.connector
import re

s3 = boto3.client('s3')


def snowflake_connector(wh='LOGSTASH_WH'):
    con = snowflake.connector.connect(
        account='ag79705',
        user='etluser',
        password='Zz123456',
        warehouse=wh,
        database='ods',
        timezone='UTC'
    )
    return con


def read_csv_to_df(global_params, read_csv_params):
    """read the csv file to dataframe. use config if necessary"""
    if global_params["src_caller"] == "manual_handler":
        file_path = os.path.join(global_params["bucket_name"], global_params["file_key"])
        df = pd.read_csv(file_path, **read_csv_params)

    else:
        obj = s3.get_object(Bucket=global_params["bucket_name"], Key=global_params["file_key"])
        df = pd.read_csv(io.BytesIO(obj['Body'].read()), **read_csv_params)

    return df


def get_domain_of_url(url):
    """ Gets an URL and returns its domain. (example: 'www.google.com/?#q=house' --> 'google.com') """
    if url and isinstance(url, str):
        # if the url is i.p. - return the i.p. without parsing
        if re.search("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", url) is not None:
            return url

        if not (url.startswith('http') or url.startswith('//')):
            url = "http://{0}".format(url)
        url = urllib.request.urlparse(url).hostname
        if url:
            for start_string in ['www', 'm', 'mobile']:
                if url.startswith(start_string + '.'):
                    return url[len(start_string) + 1:]
            return url
    return ''


def get_site_name_of_url(url):
    """ Gets an URL and returns its site name. (example: 'www.google.com/?#q=house' --> 'google') """
    full_domain = get_domain_of_url(url)
    if full_domain and '.' in full_domain:
        domain_parts = full_domain.split(".")
        if len(domain_parts) > 2 and domain_parts[-2] in ['co', 'ac', 'gov', 'org', 'net', 'go', 'com', 'edu', 'biz']:
            domain_parts.pop(-2)
        return domain_parts[-2]
    return ''


def write_to_db(df, params):
    """
    write dataframe to snowflake database according to config(table name, table column names)
    write the data to database in batches of 10,000 rows (due to cursor writing limit)
    :param df:
    :param params:
    :return:
    """
    con = snowflake_connector()
    data_lst = df.values.tolist()
    values_str = ",".join(['%s'] * len(params["cols_lst"]))
    cols_str = ",".join(params["cols_lst"])

    cur = con.cursor()
    query = "insert into " + params["tbl_name"] + "(" + cols_str + ")  values(" + values_str + ")"

    for i in range(0, len(data_lst), 10000):
        if i + 10000 < len(data_lst):
            cur.executemany(query, data_lst[i:i + 10000])
        else:
            cur.executemany(query, data_lst[i:len(data_lst) + 1])

    cur.close()
    con.close()
