pipeline {
  agent any
  stages {
    stage('Pre') {
      steps {
        // handle build from commit ID
        sh """
        if [ "${params.COMMIT_ID}" != "" ]
        then
          git checkout -B ${params.COMMIT_ID}_jenkins
          git checkout ${params.COMMIT_ID} .
          git add .
          git commit -am "jenkins build from commit id ${params.COMMIT_ID}"
        fi
        """
        // versioning
        sh """#!/bin/bash -xe
        CURRENT_VERSION=\$(head -n 1 version.txt)
        semver=( \${CURRENT_VERSION//./\\ } )
        case ${params.VERSION_TYPE} in
            patch)
            NEW_VERSION="\$((semver[0])).\$((semver[1])).\$((semver[2]+1))"
            ;;
            minor)
            NEW_VERSION="\$((semver[0])).\$((semver[1]+1)).0"
            ;;
            major)
            NEW_VERSION="\$((semver[0]+1)).0.0"
            ;;
            *)
            NEW_VERSION=${params.VERSION_TYPE}
        esac
        echo \$NEW_VERSION > version.txt
        git add .
        git commit -am \$NEW_VERSION
        """
        script {
          env.SNAPSHOTS_BUCKET = "build-snapshots"
          env.APP_VERSION = sh (script: "cat version.txt", returnStdout: true).trim()
          if (params.COMMIT_ID != '') {
            env.BRANCH_NAME = "${params.COMMIT_ID}_jenkins"
          }
          env.VERSION_ID = "${env.APP_VERSION}-${env.BRANCH_NAME}"
        }
      }
    }
    stage('App Params') {
      steps {
        script {
          env.APP_NAME = "video_external_data_importer"
          env.BUILD_DIR = "${pwd()}/build"
        }
      }
    }
    stage('Build') {
      steps {
        sh """
          pip3 install -r requirements.txt -t ${env.BUILD_DIR}
          cp -r ./src ${env.BUILD_DIR}
          cp ./version.txt ${env.BUILD_DIR}
        """
      }
    }
    stage('Test') {
      steps {
        echo 'no tests!'
      }
    }
    stage('Deploy') {
      steps {
        script {
          env.BUILD_ZIP = "${pwd()}/build.zip"
        }
        echo 'Zip build directory'
        sh """
          cd ${env.BUILD_DIR}
          zip -r9 ${env.BUILD_ZIP} .
          aws s3 cp ${env.BUILD_ZIP} s3://${env.SNAPSHOTS_BUCKET}/${env.APP_NAME}/${env.VERSION_ID}.zip
          cd -
        """
        echo 'Upload build to lambda'
        sh """
          LAMBDA_ARN="arn:aws:lambda:${params.REGION}:156785876661:function:${params.LAMBDA_NAME}"
          aws lambda update-function-code --function-name \$LAMBDA_ARN --dry-run  --s3-bucket ${env.SNAPSHOTS_BUCKET} --s3-key ${env.APP_NAME}/${env.VERSION_ID}.zip --region ${params.REGION}
        """
        sh ""
      }
    }
    stage('Post') {
      steps {
        echo 'Save build copy in s3'
        sh """
          aws s3 cp ${env.BUILD_ZIP} s3://${env.SNAPSHOTS_BUCKET}/${env.APP_NAME}/${env.VERSION_ID}.zip
        """

        echo 'Update app version on repository'
        sshagent(credentials: ['bitbucket-jenkins']) {
          script {
            if (params.COMMIT_ID == '') {
              sh (script: "git push origin HEAD:${env.BRANCH_NAME}")
            } else {
              sh (script: "git push --set-upstream origin ${env.BRANCH_NAME}:${env.VERSION_ID}")
            }
          }
        }

        echo 'Send slack realese message'
        wrap([$class: 'BuildUser']) {
          slackSend(channel: "${params.SLACK_CHANNEL}", color: 'good',
                    message: "*${env.APP_NAME}@${env.VERSION_ID}* has been deployed!\n*User:* ${BUILD_USER}\n*Description:*\n${params.VERSION_CHANGELOG}")
        }
      }
    }
  }
  parameters {
    choice(name: 'VERSION_TYPE', choices: ['patch', 'minor', 'major'], description: 'The version type')
    string(name: 'COMMIT_ID', defaultValue: '', description: 'Build from commit (HEAD if empty)')
    string(name: 'SLACK_CHANNEL', defaultValue: '#bi-changelog', description: 'The tracking slack channel')
    text(name: 'VERSION_CHANGELOG', defaultValue: '', description: 'The version changelog description')
    string(name: 'LAMBDA_NAME', defaultValue: 'video_external_data_importer', description: 'The lambda function name')
    string(name: 'REGION', defaultValue: 'us-west-2', description: 'The lambda region')
  }
}